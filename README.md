# pthsem 2.0.8

This is pthsem that is used from [bcusdk](https://www.auto.tuwien.ac.at/~mkoegler/index.php/bcusdk)

Build/install pthsem
----
pthsem is needed for bcusdk.

```sh
cd pthsem-2.0.8
./configure
make
sudo make install
```

